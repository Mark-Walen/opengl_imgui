from typing import Any
from imgui.integrations.glfw import GlfwRenderer
from pyopenglutils.window import Window, before_init, after_init, pre_destroy
from OpenGL.GL import *

import glfw
import glm
import imgui
import numpy as np
import logging as log

impl: GlfwRenderer
cn_font: Any
program: Any
vao1: np.ndarray
vao2: np.ndarray
line_width = 2.0
background_color = glm.vec4(0.8, 0.8, 0.8, 1.0)
opengl_log = log.getLogger('OpenGL')
imgui_log = log.getLogger('imgui')


def create_vao_1():
    line_position = np.array([
        -0.8, 0.0, 0.0,
        0.8, 0.0, 0.0
    ], dtype=GLfloat)
    line_position_vbo = np.empty(1, dtype=GLuint)
    glCreateBuffers(1, line_position_vbo)
    glNamedBufferStorage(line_position_vbo[0], line_position.nbytes, line_position, GL_DYNAMIC_STORAGE_BIT)

    line_color = np.array([
        1.0, 0.0, 0.0,
        1.0, 0.0, 0.0
    ], dtype=GLfloat)
    line_color_vbo = np.empty(1, dtype=GLuint)
    glCreateBuffers(1, line_color_vbo)
    glNamedBufferStorage(line_color_vbo[0], line_color.nbytes, line_color, GL_DYNAMIC_STORAGE_BIT)

    _vao = np.empty(1, dtype=GLuint)
    glCreateVertexArrays(1, _vao)

    line_position_binding_point = 4
    glVertexArrayVertexBuffer(_vao[0], line_position_binding_point, line_position_vbo[0], 0, 3 * line_position.itemsize)
    glVertexArrayAttribBinding(_vao[0], 0, line_position_binding_point)
    glVertexArrayAttribFormat(_vao[0], 0, 3, GL_FLOAT, False, 0)
    glEnableVertexArrayAttrib(_vao[0], 0)

    line_color_binding_point = 6
    glVertexArrayVertexBuffer(_vao[0], line_color_binding_point, line_color_vbo[0], 0, 3 * line_color.itemsize)
    glVertexArrayAttribBinding(_vao[0], 1, line_color_binding_point)
    glVertexArrayAttribFormat(_vao[0], 1, 3, GL_FLOAT, False, 0)
    glEnableVertexArrayAttrib(_vao[0], 1)

    return _vao


def create_vao_2(vertices):
    line_vertices = np.array(vertices, dtype=GLfloat)
    line_vbo = np.empty(1, dtype=GLuint)
    glCreateBuffers(1, line_vbo)
    glNamedBufferStorage(line_vbo[0], line_vertices.nbytes, line_vertices, GL_DYNAMIC_STORAGE_BIT)

    _vao = np.empty(1, dtype=GLuint)
    glCreateVertexArrays(1, _vao)
    line_binding_point = 9
    glVertexArrayVertexBuffer(_vao[0], line_binding_point, line_vbo[0], 0, 6 * sizeof(GLfloat))
    glVertexArrayAttribBinding(_vao[0], 0, line_binding_point)
    glVertexArrayAttribFormat(_vao[0], 0, 3, GL_FLOAT, False, 0)
    glEnableVertexArrayAttrib(_vao[0], 0)

    glVertexArrayAttribBinding(_vao[0], 1, line_binding_point)
    glVertexArrayAttribFormat(_vao[0], 1, 3, GL_FLOAT, False, 3 * sizeof(GLfloat))
    glEnableVertexArrayAttrib(_vao[0], 1)

    return _vao


@before_init()
def glfw_hint_config(w: Window):
    """
    :param w: Window instance
    :return: None
    """
    glfw.window_hint(glfw.CONTEXT_VERSION_MAJOR, 4)
    glfw.window_hint(glfw.CONTEXT_VERSION_MINOR, 5)
    glfw.window_hint(glfw.OPENGL_PROFILE, glfw.OPENGL_CORE_PROFILE)


@after_init()
def imgui_config(w: Window):
    """
    :param w: Window instance
    :return: None
    """
    global impl, cn_font
    glfw.set_window_pos(w.window, 100, 100)
    glfw.make_context_current(w.window)
    imgui.create_context()
    impl = GlfwRenderer(w.window, False)
    io = imgui.get_io()
    fonts = io.fonts
    cn_font = io.fonts.add_font_from_file_ttf("C:/Windows/Fonts/simhei.ttf", 16, None,
                                              fonts.get_glyph_ranges_chinese_full())
    impl.refresh_font_texture()


@after_init()
def opengl_vertexes_config(w: Window):
    global program, vao1, vao2
    vertex_shader_code = """
    #version 450 core
    layout (location = 0) in vec3 position;
    layout (location = 1) in vec3 color;
    out vec3 vertex_color;
    void main()
    {
        gl_Position = vec4(position, 1.0);
        vertex_color = color;
    }
    """

    fragment_shader_code = """
    #version 450 core
    in vec3 vertex_color;
    out vec4 fragment_color;
    void main()
    {
        fragment_color = vec4(vertex_color, 1.0);
    }
    """
    vertex_shader = glCreateShader(GL_VERTEX_SHADER)
    glShaderSource(vertex_shader, vertex_shader_code)
    glCompileShader(vertex_shader)

    fragment_shader = glCreateShader(GL_FRAGMENT_SHADER)
    glShaderSource(fragment_shader, fragment_shader_code)
    glCompileShader(fragment_shader)

    program = glCreateProgram()
    glAttachShader(program, vertex_shader)
    glAttachShader(program, fragment_shader)
    glLinkProgram(program)

    glDeleteShader(vertex_shader)
    glDeleteShader(fragment_shader)

    vertices_1 = [
        -0.8, 0.0, 0.0, 1.0, 0.0, 0.0,
        0.8, 0.0, 0.0, 1.0, 0.0, 0.0
    ]

    vertices_2 = [
        0.0, -0.8, 0.0, 0.0, 1.0, 0.0,
        0.0, 0.8, 0.0, 0.0, 1.0, 0.0
    ]
    vao1 = create_vao_1()
    # vao1 = create_vao_2(vertices_1)
    vao2 = create_vao_2(vertices_2)


def imgui_main():
    global line_width, background_color
    impl.process_inputs()
    imgui.new_frame()
    imgui.push_font(cn_font)
    imgui.set_next_window_position(0, 0, condition=imgui.FIRST_USE_EVER)
    imgui.set_next_window_size(200, 180, condition=imgui.FIRST_USE_EVER)
    imgui.begin("Settings...")
    _, new_width = imgui.slider_float("line width", line_width, 2.0, 10.0)
    _, color = imgui.color_edit3('background color', *background_color.to_tuple()[:-1])
    background_color = glm.vec4(*color, 1)

    line_width = new_width
    imgui.end()
    imgui.pop_font()
    imgui.render()
    impl.render(imgui.get_draw_data())


def opengl_main():
    global program, background_color, line_width, vao1, vao2

    glClearNamedFramebufferfv(0, GL_COLOR, 0, background_color.to_tuple())

    # rendering
    glUseProgram(program)
    glLineWidth(line_width)

    glBindVertexArray(vao1[0])
    glDrawArrays(GL_LINES, 0, 2)

    glBindVertexArray(vao2[0])
    glDrawArrays(GL_LINES, 0, 2)

    # display imgui
    imgui_main()


@pre_destroy()
def clean_up(w):
    opengl_log.info('clean resource...')
    glDeleteVertexArrays(1, vao1[0])
    glDeleteVertexArrays(1, vao2[0])
    glDeleteProgram(program)
    impl.shutdown()
    glfw.destroy_window(w.window)
    opengl_log.info('down')


def main():
    log.basicConfig(
        format="%(asctime)s %(filename)s:%(lineno)d [%(levelname)s]:%(message)s", datefmt="[%b %d %H:%M:%S]",
        level=log.DEBUG)
    w = Window(800, 600, "Hello, Point")
    w.create_window()

    w.main_loop(opengl_main)

    w.close_window()


if __name__ == '__main__':
    main()
