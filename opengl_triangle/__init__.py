from typing import Any
from imgui.integrations.glfw import GlfwRenderer
from pyopenglutils import Window, before_init, after_init, pre_destroy, ProgramVF, VBO, VAO, VertexAttrib
from OpenGL.GL import *

import glfw
import glm
import imgui
import numpy as np
import logging as log

impl: GlfwRenderer
cn_font: Any
program: ProgramVF
opengl_obj_table = []
background_color = glm.vec4(0.8, 0.8, 0.8, 1.0)
color = (1.0, 1.0, 1.0)
x_offset = 0.0
x_increment = 0.0002

opengl_log = log.getLogger('OpenGL')
imgui_log = log.getLogger('imgui')


def create_vao(vertices):
    _vbo = VBO(np.array(vertices, dtype=GLfloat))
    _vao = VAO()
    pos_attrib = VertexAttrib('pos', 0, 3, GL_FLOAT, False, 0)
    color_attrib = VertexAttrib('color', 1, 3, GL_FLOAT, False, 3 * sizeof(GLfloat))

    binding_point = 0
    _vao.set_vertex_buffer(_vbo, binding_point, 0, 6 * sizeof(GLfloat))

    _vao.set_vertex_attrib(binding_point, pos_attrib)
    _vao.set_vertex_attrib(binding_point, color_attrib)

    return _vbo, _vao


@before_init()
def glfw_hint_config(w: Window):
    """
    :param w: Window instance
    :return: None
    """
    glfw.window_hint(glfw.CONTEXT_VERSION_MAJOR, 4)
    glfw.window_hint(glfw.CONTEXT_VERSION_MINOR, 5)
    glfw.window_hint(glfw.OPENGL_PROFILE, glfw.OPENGL_CORE_PROFILE)


@after_init()
def imgui_config(w: Window):
    """
    :param w: Window instance
    :return: None
    """
    global impl, cn_font
    glfw.set_window_pos(w.window, 100, 100)
    glfw.make_context_current(w.window)
    imgui.create_context()
    impl = GlfwRenderer(w.window, False)
    io = imgui.get_io()
    fonts = io.fonts
    cn_font = io.fonts.add_font_from_file_ttf("C:/Windows/Fonts/simhei.ttf", 16, None,
                                              fonts.get_glyph_ranges_chinese_full())
    impl.refresh_font_texture()


@after_init()
def opengl_vertexes_config(w: Window):
    import os

    global program, opengl_obj_table

    cur_dir = os.path.dirname(__file__)
    program = ProgramVF(f'{cur_dir}/triangle.vert', f'{cur_dir}/triangle.frag')
    program.load_shader()
    vertices = [
        # position          # color
        0.0, 0.25, 0.0,     1.0, 0.0, 0.0,
        0.25, -0.25, 0.0,   0.0, 1.0, 0.0,
        -0.25, -0.25, 0.0,  0.0, 0.0, 1.0
    ]
    opengl_obj_table += list(create_vao(vertices))


def imgui_main():
    global background_color, color
    impl.process_inputs()
    imgui.new_frame()
    imgui.push_font(cn_font)
    imgui.set_next_window_position(0, 0, condition=imgui.FIRST_USE_EVER)
    imgui.set_next_window_size(200, 180, condition=imgui.FIRST_USE_EVER)
    imgui.begin("Settings...")
    _, color = imgui.color_edit3('light color', *color)
    _, _color = imgui.color_edit3('background color', *background_color.to_tuple()[:-1])
    background_color = glm.vec4(*_color, 1.0)

    imgui.end()
    imgui.pop_font()
    imgui.render()
    impl.render(imgui.get_draw_data())


def opengl_main():
    global program, background_color, color, opengl_obj_table, x_offset, x_increment

    glClearBufferfv(GL_COLOR, 0, background_color.to_tuple())

    # rendering
    program.use()
    x_offset += x_increment
    if x_offset < -0.7:
        x_increment = -x_increment
    if x_offset > 0.7:
        x_increment = -x_increment

    glProgramUniform1f(program.id, 0, x_offset)
    glProgramUniform3f(program.id, 1, *color)

    for obj in opengl_obj_table:
        if not isinstance(obj, VAO):
            continue
        obj.bind()
        glDrawArrays(GL_TRIANGLES, 0, 3)

    # display imgui
    imgui_main()


@pre_destroy()
def clean_up(w):
    opengl_log.info('clean resource...')
    for obj in opengl_obj_table:
        obj.delete()
    program.delete()
    impl.shutdown()
    glfw.destroy_window(w.window)
    opengl_log.info('down')


def main():
    log.basicConfig(
        format="%(asctime)s %(filename)s:%(lineno)d [%(levelname)s]:%(message)s", datefmt="[%b %d %H:%M:%S]",
        level=log.DEBUG)
    w = Window(800, 600, "Hello, Triangle")
    w.create_window()

    w.main_loop(opengl_main)

    w.close_window()


if __name__ == '__main__':
    main()
