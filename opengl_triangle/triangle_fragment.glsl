#version 450 core
in vec3 vertex_color;
out vec4 fragment_color;
layout (location = 1) uniform vec3 color;

void main()
{
    fragment_color = vec4(color * vertex_color, 1.0);
}
