from typing import Any
from imgui.integrations.glfw import GlfwRenderer
from pyopenglutils.window import Window, before_init, after_init
from OpenGL.GL import *

import glfw
import glm
import imgui

impl: GlfwRenderer
cn_font: Any
background_color = glm.vec4(0.8, 0.8, 0.8, 1.0)


@before_init()
def glfw_hint_config(w: Window):
    """
    :param w: Window instance
    :return: None
    """
    glfw.window_hint(glfw.CONTEXT_VERSION_MAJOR, 4)
    glfw.window_hint(glfw.CONTEXT_VERSION_MINOR, 5)
    glfw.window_hint(glfw.OPENGL_PROFILE, glfw.OPENGL_CORE_PROFILE)


@after_init()
def imgui_config(w: Window):
    """
    :param w: Window instance
    :return: None
    """
    global impl, cn_font
    imgui.create_context()
    impl = GlfwRenderer(w.window, False)
    io = imgui.get_io()
    fonts = io.fonts
    cn_font = io.fonts.add_font_from_file_ttf("C:/Windows/Fonts/simhei.ttf", 16, None,
                                              fonts.get_glyph_ranges_chinese_full())
    impl.refresh_font_texture()


def opengl_main():
    global background_color
    glClearNamedFramebufferfv(0, GL_COLOR, 0, background_color.to_tuple())
    impl.process_inputs()
    imgui.new_frame()
    imgui.push_font(cn_font)
    imgui.set_next_window_position(0, 0, condition=imgui.FIRST_USE_EVER)
    imgui.set_next_window_size(300, 180, condition=imgui.FIRST_USE_EVER)
    imgui.begin("设置背景颜色")
    imgui.text("这是一个可以交互的演示程序：")
    _, color = imgui.color_edit3("颜色", background_color.x,
                                 background_color.y, background_color.z)
    background_color = glm.vec4(color[0], color[1], color[2], 1.0)
    imgui.end()
    imgui.pop_font()
    imgui.render()
    impl.render(imgui.get_draw_data())


def main():
    w = Window(800, 600, "Hello, ImGui")
    w.create_window()
    w.main_loop(opengl_main)
    w.close_window()


if __name__ == '__main__':
    main()
