#version 450 core
in vec3 vertex_color;
out vec4 fragment_color;
layout (location = 0) uniform vec3 line_color;
layout (location = 1) uniform int wireframe = 0;

void main()
{
    if (wireframe == 0 )
    {
        fragment_color = vec4(vertex_color, 1.0);
    }
    else
    {
        fragment_color = vec4(line_color, 1.0);
    }
}