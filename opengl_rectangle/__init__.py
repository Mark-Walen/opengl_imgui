from typing import Any
from imgui.integrations.glfw import GlfwRenderer
from pyopenglutils import *
from OpenGL.GL import *

import glfw
import glm
import imgui
import numpy as np
import logging as log

impl: GlfwRenderer
cn_font: Any
program: ProgramVF
opengl_obj_table = []
background_color = glm.vec4(00, 00, 00, 1.0)
line_color = glm.vec3(1.0, 1.0, 1.0)
x_offset = 0.0
x_increment = 0.0002
indices = np.array([0, 1, 3,
                    1, 2, 3,
                    2, 3, 0], dtype=GLuint)
wireframe = 0

opengl_log = log.getLogger('OpenGL')
imgui_log = log.getLogger('imgui')


def create_vao(vertices):
    _vbo = VBO(np.array(vertices, dtype=GLfloat))
    _ebo = EBO(indices)
    _vao = VAO()

    pos_attrib = VertexAttrib('pos', 0, 3, GL_FLOAT, False, 0)
    color_attrib = VertexAttrib('color', 1, 3, GL_FLOAT, False, 3 * sizeof(GLfloat))

    binding_point = 0
    _vao.set_vertex_buffer(_vbo, binding_point, 0, 6 * sizeof(GLfloat))
    glVertexArrayElementBuffer(_vao.vao_id, _ebo.ebo_id)

    _vao.set_vertex_attrib(binding_point, pos_attrib)
    _vao.set_vertex_attrib(binding_point, color_attrib)

    return _vbo, _vao, _ebo


@before_init()
def glfw_hint_config(w: Window):
    """
    :param w: Window instance
    :return: None
    """
    glfw.window_hint(glfw.CONTEXT_VERSION_MAJOR, 4)
    glfw.window_hint(glfw.CONTEXT_VERSION_MINOR, 5)
    glfw.window_hint(glfw.OPENGL_PROFILE, glfw.OPENGL_CORE_PROFILE)


@after_init()
def imgui_config(w: Window):
    """
    :param w: Window instance
    :return: None
    """
    global impl, cn_font
    glfw.set_window_pos(w.window, 100, 100)
    glfw.make_context_current(w.window)
    imgui.create_context()
    impl = GlfwRenderer(w.window, False)
    io = imgui.get_io()
    fonts = io.fonts
    cn_font = io.fonts.add_font_from_file_ttf("C:/Windows/Fonts/simhei.ttf", 16, None,
                                              fonts.get_glyph_ranges_chinese_full())
    impl.refresh_font_texture()


@after_init()
def opengl_vertexes_config(w: Window):
    import os

    global program, opengl_obj_table

    cur_dir = os.path.dirname(__file__)
    program = ProgramVF(f'{cur_dir}/rectangle.vert', f'{cur_dir}/rectangle.frag')
    program.load_shader()

    rect_width = 1.8
    rect_height = 1.8

    vertices = [
        # position          # color
        glm.vec3(rect_width / 2.0, rect_height / 2.0, 0.0), glm.vec3(0.0, 0.0, 1.0),
        glm.vec3(rect_width / 2.0, -rect_height / 2.0, 0.0), glm.vec3(0.0, 0.0, 1.0),
        glm.vec3(-rect_width / 2.0, -rect_height / 2.0, 0.0), glm.vec3(0.0, 0.0, 1.0),
        glm.vec3(-rect_width / 2.0, rect_height / 2.0, 0.0), glm.vec3(0.0, 0.0, 1.0)
    ]
    opengl_obj_table += list(create_vao(vertices))


def imgui_main():
    global wireframe, line_color
    impl.process_inputs()
    imgui.new_frame()
    imgui.push_font(cn_font)
    imgui.set_next_window_position(0, 0, condition=imgui.FIRST_USE_EVER)
    imgui.set_next_window_size(200, 180, condition=imgui.FIRST_USE_EVER)
    imgui.begin("Settings...")
    _, color = imgui.color_edit3('line color', *line_color)
    line_color = glm.vec3(*color)
    if imgui.radio_button("实体模式", wireframe == 0):
        wireframe = 0
    elif imgui.radio_button("线框模式", wireframe == 1):
        wireframe = 1
    elif imgui.radio_button("实体加线框模式", wireframe == 2):
        wireframe = 2

    imgui.end()
    imgui.pop_font()
    imgui.render()
    impl.render(imgui.get_draw_data())


def render_mesh(_program: ProgramVF, _indices, _line_color):
    _program.uniform1i(1, 0)
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
    glDrawElements(GL_TRIANGLES, len(_indices), GL_UNSIGNED_INT, None)


def render_wireframe(_program: ProgramVF, _indices, _line_color):
    _program.uniform3fv(0, 1, _line_color)
    _program.uniform1i(1, 1)
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)
    glLineWidth(5.0)
    glDrawElements(GL_TRIANGLES, len(_indices), GL_UNSIGNED_INT, None)
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
    glLineWidth(1.0)


def render_mesh_with_wireframe(_program: ProgramVF, _indices, _line_color):
    glEnable(GL_POLYGON_OFFSET_LINE)
    glPolygonOffset(1.0, 1.0)
    render_mesh(_program, _indices, _line_color)
    render_wireframe(_program, _indices, _line_color)
    glDisable(GL_POLYGON_OFFSET_LINE)


def opengl_main():
    global program, background_color, line_color, opengl_obj_table, indices

    glClearBufferfv(GL_COLOR, 0, background_color.to_tuple())

    # rendering
    program.use()
    opengl_obj_table[1].bind()

    render_funcs = [render_mesh, render_wireframe, render_mesh_with_wireframe]
    render_func = render_funcs[wireframe]
    render_func(program, indices, line_color)

    # display imgui
    imgui_main()


@pre_destroy()
def clean_up(w):
    opengl_log.info('clean resource...')
    for obj in opengl_obj_table:
        obj.delete()
    program.delete()
    impl.shutdown()
    glfw.destroy_window(w.window)
    opengl_log.info('down')


def main():
    log.basicConfig(
        format="%(asctime)s %(filename)s:%(lineno)d [%(levelname)s]:%(message)s", datefmt="[%b %d %H:%M:%S]",
        level=log.DEBUG)
    w = Window(800, 600, "Hello, Triangle")
    w.create_window()

    w.main_loop(opengl_main)

    w.close_window()


if __name__ == '__main__':
    main()
