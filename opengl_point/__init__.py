from typing import Any
from imgui.integrations.glfw import GlfwRenderer
from pyopenglutils.window import Window, before_init, after_init, pre_destroy
from OpenGL.GL import *

import glfw
import glm
import imgui
import numpy as np

impl: GlfwRenderer
cn_font: Any
program: Any
point_size = 20.0
background_color = glm.vec4(0.8, 0.8, 0.8, 1.0)
vao = np.empty(1, dtype=GLuint)


@before_init()
def glfw_hint_config(w: Window):
    """
    :param w: Window instance
    :return: None
    """
    glfw.window_hint(glfw.CONTEXT_VERSION_MAJOR, 4)
    glfw.window_hint(glfw.CONTEXT_VERSION_MINOR, 5)
    glfw.window_hint(glfw.OPENGL_PROFILE, glfw.OPENGL_CORE_PROFILE)


@after_init()
def imgui_config(w: Window):
    """
    :param w: Window instance
    :return: None
    """
    global impl, cn_font
    glfw.set_window_pos(w.window, 100, 100)
    glfw.make_context_current(w.window)
    imgui.create_context()
    impl = GlfwRenderer(w.window, False)
    io = imgui.get_io()
    fonts = io.fonts
    cn_font = io.fonts.add_font_from_file_ttf("C:/Windows/Fonts/simhei.ttf", 16, None,
                                              fonts.get_glyph_ranges_chinese_full())
    impl.refresh_font_texture()


@after_init()
def opengl_shader_config(w: Window):
    global program
    vertex_shader_code = """
    #version 450 core
    void main()
    {
        gl_Position = vec4(0.0, 0.0, 0.0, 1.0);
    }
    """
    fragment_shader_code = """
    #version 450 core
    out vec4 fragment_color;
    void main()
    {
        fragment_color = vec4(0.0, 0.0, 1.0, 1.0);
    }
    """
    vertex_shader = glCreateShader(GL_VERTEX_SHADER)
    glShaderSource(vertex_shader, vertex_shader_code)
    glCompileShader(vertex_shader)

    fragment_shader = glCreateShader(GL_FRAGMENT_SHADER)
    glShaderSource(fragment_shader, fragment_shader_code)
    glCompileShader(fragment_shader)

    program = glCreateProgram()
    glAttachShader(program, vertex_shader)
    glAttachShader(program, fragment_shader)
    glLinkProgram(program)

    glDeleteShader(vertex_shader)
    glDeleteShader(fragment_shader)


def opengl_main():
    global program, background_color, point_size, vao

    glClearNamedFramebufferfv(0, GL_COLOR, 0, background_color.to_tuple())

    # rendering
    glUseProgram(program)
    glBindVertexArray(vao[0])
    glPointSize(point_size)
    glDrawArrays(GL_POINTS, 0, 1)

    # display imgui
    impl.process_inputs()
    imgui.new_frame()
    imgui.push_font(cn_font)
    imgui.set_next_window_position(0, 0, condition=imgui.FIRST_USE_EVER)
    imgui.set_next_window_size(300, 180, condition=imgui.FIRST_USE_EVER)
    imgui.begin("Change point size")
    _, new_size = imgui.slider_float("point size", point_size, 2.0, 80.0)
    point_size = new_size
    imgui.end()
    imgui.pop_font()
    imgui.render()
    impl.render(imgui.get_draw_data())


@pre_destroy()
def clean_up(w):
    glDeleteVertexArrays(1, vao[0])
    glDeleteProgram(program)
    impl.shutdown()
    glfw.destroy_window(w.window)


def main():
    w = Window(800, 600, "Hello, Point")
    w.create_window()

    glCreateVertexArrays(1, vao)
    w.main_loop(opengl_main)

    w.close_window()


if __name__ == '__main__':
    main()
